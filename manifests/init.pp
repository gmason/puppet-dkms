# == Class: dkms
#
# This module installs and configures DKMS. Unnecessary for newer 
# puppet-zfs versions
#
# === Parameters
#
# None.
#
# === Variables
#
# None.
#
# === Examples
#
#  class { dkms: }
#
# === Authors
#
# Greg Mason <gmason@msu.edu>
# Arnaud Gomes-do-Vale <Arnaud.Gomes@ircam.fr>
#
# === Copyright
#
# Copyright 2014 Michigan State University Board of Trustees
# Copyright 2013 Arnaud Gomes-do-Vale
#
class dkms {

  include gcc

  case $::operatingsystem {
    'RedHat', 'CentOS', 'Scientific': {
      # We need kernel-devel for DKMS.
      if !defined(Package['kernel-devel']) {
        package { 'kernel-devel':
          ensure => present,
          before => Package['dkms'],
        }
      }

      package { 'dkms':
        ensure => present,
      }
      service { 'dkms_autoinstaller':
        enable  => true,
        require => Class['gcc'],
      }
    }
    'Debian', 'Ubuntu': {
      package { 'dkms':
        ensure => present,
      }
      package { 'linux-headers-server':
        ensure => present,
      }
      package { "linux-headers-$kernelrelease":
        ensure => present,
      }
    }
    default: {
      fail("Module ${module_name} is not supported on ${::operatingsystem}.")
    }
  }

}
